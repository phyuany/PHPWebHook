# 服务器Git代码同步钩子

## 一、概述

### 1.1 项目地址

Gitee 码云项目地址：<https://gitee.com/phyuany/PHPWebHook>

### 1.2 使用场景

Git的Webhook是一种事件通知机制，它允许你在特定的Git操作发生时触发自定义的操作或通知。下面是一些常见的Git Webhook的使用场景：

- **自动构建和部署**：当代码库中的代码发生变更时，可以使用Webhook触发自动构建和部署流程。这对于持续集成和持续部署（CI/CD）非常有用，可以确保新的代码变更被自动构建、测试和部署到生产环境中。

- **代码质量检查**：在代码提交或推送到远程仓库时，可以使用Webhook触发代码质量检查工具，例如静态代码分析工具、单元测试等。这样可以在代码合并到主分支之前，自动检查代码的质量和健壮性。

- **通知和警报**：当有新的代码提交或推送到远程仓库时，可以使用Webhook触发通知和警报，例如发送电子邮件、Slack消息或短信通知团队成员。这样可以及时地通知团队成员有关代码变更的信息。

- **自动化任务**：通过Webhook，可以触发各种自动化任务，例如生成文档、更新缓存、同步数据等。这样可以减少手动操作，并确保在代码变更时自动执行必要的任务。

### 1.3 实现目标

本文使用PHP语言来编写一个能实现PHP项目自动部署的程序。这个流程可以实现以下目标：

- 实现代码同步功能，将代码从码云仓库同步到业务服务器；
- 实现邮件通知功能，当代码同步完成之后，发送通知邮件给代码推送者。

### 1.4 项目结构

项目的关键代码如下：

```shell script
.
├── README.md
├── index.php
├── MailSender.php
└── vendor
    └── autoload.php
```

- `index.php`: 主程序入口文件，负责接收用户提交的表单数据，并调用MailSender类发送邮件。
- `MailSender.php`: 负责发送邮件的类，使用PHPMailer库实现邮件发送功能。
- `vendor`: 存放PHPMailer依赖库的目录。

## 二、实现过程

### 2.1 初始化项目

创建一个空的项目目录，在目录之下使用composer安装一个phpmailer邮件发送依赖库，composer指令如下：

```shell
composer require phpmailer/phpmailer
```

### 2.2 定义邮件发送者对象

在项目根目录创建MailSender.php文件，首先在头部引入在1中安装的phpmailer依赖，如下：

```php
<?php
require_once 'vendor/autoload.php';

// 引入phpmailer依赖
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
```

在`MailSender.php`文件中添加`MailSender`类，并设置SMTP邮件发送的相关参数，如下代码：

```php
class MailSender
{
    /*
     * SMTP配置
     * */
    private $smtp_host = 'smtp.exmail.qq.com';//SMTP服务器地址
    private $smtp_from = '极客开发者-管理员';//发送者
    private $smtp_username = 'admin@jkdev.cn';//邮箱账号
    private $smtp_password = '******';//邮箱密码
    private $smtp_port = '465';//端口号
}
```

再创建是实例化邮件发送者的方法obtainEmailSender，第一个参数是邮件发送的目标邮箱数组（也就是说，可以同时将通知邮件发到多个目标邮箱），第二个参数代表发送主题，第三代表邮件内容，如下代码

```php
public function obtainEmailSender(array $addresses, $subject, $body)
    {
        $mailSender = new PHPMailer(true);
        $mailSender->CharSet = 'UTF-8';

        //Server settings
        $mailSender->SMTPDebug = SMTP::DEBUG_SERVER;                       // Enable verbose debug output
        $mailSender->isSMTP();                                             // Send using SMTP
        $mailSender->Host = $this->smtp_host;                              // Set the SMTP server to send through
        $mailSender->SMTPAuth = true;                                      // Enable SMTP authentication
        $mailSender->Username = $this->smtp_username;                      // SMTP username
        $mailSender->Password = $this->smtp_password;                      // SMTP password
        $mailSender->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;             // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $mailSender->Port = $this->smtp_port;                              // TCP port to connect to

        //Recipients
        $mailSender->setFrom($this->smtp_username, $this->smtp_from);
        foreach ($addresses as $index => $address) {
            $mailSender->addAddress($address);                             // Name is optional
        }

        // Content
        $mailSender->isHTML(true);                                  // Set email format to HTML
        $mailSender->Subject = $subject;
        $mailSender->Body = $body;

        //返回邮件对象
        return $mailSender;
    }
```

### 2.3 定义程序入口文件

创建钩子入口文件`index.php`，并引入`MailSender.php`文件，如下代码：

```php
<?php
include 'MailSender.php';
```

首先验证提交者是否将代码提交到master分支，其次验证密码是否正确。如果验证不通过时，直接退出程序，如下代码：

```php
// 检测IP
if (!in_array($_SERVER['REMOTE_ADDR'], $allowIpArr)) {
    echo '非法IP:' . $_SERVER['REMOTE_ADDR'];
    exit(0);
}

// 获取请求参数
$headers = getallheaders();
$body = json_decode(file_get_contents("php://input"), true);
// 请求密码
$password = 'www.jkdev.cn';

// 验证提交分支是否为master
if (!isset($body['ref']) || $body['ref'] !== 'refs/heads/master') {
    echo '非主分支' . $body;
    exit(0);
}

// 验证提交密码是否正确
if (!isset($body['password']) || $body['password'] !== $password) {
    echo '密码错误';
    exit(0);
}
```

通过验证之后，在服务器拉取git服务器上的最新代码

```php
// 验证成功，拉取代码
$path = $body['project']['path'];
$command = 'cd /var/www/html/' . $path . ' && git pull 2>&1';
$res = shell_exec($command);
```

在以上代码中，先使用`cd`命令进入服务器上的项目目录，这里要注意，项目后缀路径必须和`git`服务器上的项目路径是一致的。再使用`git pull`命令拉取代码，使用`2>&1`指令会返回`git`执行结果。最后使用`shell_exec`执行命令并使用`$res`变量来接收执行结果。
最后，定义通知邮件发送内容，代码如下：

```php
// 发送邮件
$addresses = [
    $body['sender']['email'],// 将邮件发送给发送者
    $body['repository']['owner']['email']// 将邮件发送给仓库所有者
];
// 去除重复的内容
$addresses = array_unique($addresses);

try {
    // 更新说明
    $title = '部署成功通知';
    // 构造邮件内容
    $message = $body['head_commit']['message'];// 提交信息
    $datetime = date('Y-m-d H:i:s', $body['timestamp'] / 1000);// 时间
    $pusher = $body['pusher']['name'];// 提交人
    $name = $body['project']['name'];// 项目名
    $path = $body['project']['path'];// 路径
    $content = <<<HTML
<html>
<body>
    <h2>{$body['project']['name']}已部署成功</h2>
    <p>
    描述：<span style="font-size: 16px; color: cadetblue">$message</span> <br>
    时间：<span style="font-size: 16px; color: red">$datetime</span> <br>
    提交人：<span style="font-size: 16px; color: cadetblue">$pusher</span> <br>
    项目名称：<span style="font-size: 16px; color: cadetblue">$name</span> <br> 
    项目路径：<span style="font-size: 16px; color: cadetblue">$path</span>
    </p>
</body>
</html>
HTML;

    // 发送邮件
    $emailSender = (new MailSender())->obtainEmailSender($addresses, $title, $content);
    $emailSender->send();
    // 返回结果
    echo '邮件发送成功，git pull执行结果：' . $res;
} catch (\PHPMailer\PHPMailer\Exception $e) {
    echo '邮件发送失败，git pull执行结果：' . $res . '，邮件日志：' . $e;
}
```

在以上代码中，我们使用代码推送者和仓库所有者作为目标邮件通知对象。如果两个目标是同一个邮箱，将只取一个。其次构造邮件发送内容，使用邮件发送者的send方法进行邮件发送。最终，将git拉取结果和邮件发送结果响应给请求者。

## 三、总结

本文结合[码云](https://gitee.com)的网络钩子(WebHook)功能，使用PHP代码编写了一个HTTP接口，当开发者往[码云](https://gitee.com)上提交代码时，将触发钩子携带相关信息去调用业务服务器接口，从而我们可以在业务服务器上触发shell命令去同步git服务器上的代码，达到自动部署的目的。你还可以参考码云网络钩子的文档，进而进行代码改进，实现其他的网络钩子相关的业务需求！

- 参考链接：[https://gitee.com/help](https://gitee.com/help)
- 本文原创首发自公众号：[极客开发者](https://phy.xyz/images/wechat.jpg)，

>原创文章，禁止转载！
